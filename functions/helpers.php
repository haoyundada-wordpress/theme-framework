<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access directly.
/**
 *
 * Array search key & value
 *
 * @since 1.0.0
 * @version 1.0.0
 *
 */
if ( ! function_exists( 'csf_array_search' ) ) {
  function csf_array_search( $array, $key, $value ) {

    $results = array();

    if ( is_array( $array ) ) {
      if ( isset( $array[$key] ) && $array[$key] == $value ) {
        $results[] = $array;
      }

      foreach ( $array as $sub_array ) {
        $results = array_merge( $results, csf_array_search( $sub_array, $key, $value ) );
      }

    }

    return $results;

  }
}

/**
 *
 * Between Microtime
 *
 * @since 1.0.0
 * @version 1.0.0
 *
 */
if ( ! function_exists( 'csf_timeout' ) ) {
  function csf_timeout( $timenow, $starttime, $timeout = 30 ) {
    return ( ( $timenow - $starttime ) < $timeout ) ? true : false;
  }
}

/**
 *
 * Check for wp editor api
 *
 * @since 1.0.0
 * @version 1.0.0
 *
 */
if ( ! function_exists( 'csf_wp_editor_api' ) ) {
  function csf_wp_editor_api() {
    global $wp_version;
    return version_compare( $wp_version, '4.8', '>=' );
  }
}


/**
 * Fires before the specified template part file is loaded.
 *
 * The dynamic portion of the hook name, `$slug`, refers to the slug name
 * for the generic template part.
 *
 * @param string $slug The slug name for the generic template.
 * @param string|null $name The name of the specialized template or null if
 *                          there is none.
 * @param array $args Additional arguments passed to the template.
 * @since 5.5.0 The `$args` parameter was added.
 *
 * @since 3.0.0
 */
if ( ! function_exists( 'csf_get_template_part' ) ) {
    function csf_get_template_part($slug, $name = null, $args = array(),$load_once=false)
    {
        do_action("wp_get_template_part_{$slug}", $slug, $name, $args,$load_once);

        $templates = array();
        $name = (string)$name;
        if ('' !== $name) {
            $templates[] = "{$slug}/{$name}.php";
        }

        $templates[] = "{$slug}.php";

        /**
         * Fires before an attempt is made to locate and load a template part.
         *
         * @param string $slug The slug name for the generic template.
         * @param string $name The name of the specialized template or an empty
         *                            string if there is none.
         * @param string[] $templates Array of template files to search for, in order.
         * @param array $args Additional arguments passed to the template.
         * @since 5.2.0
         * @since 5.5.0 The `$args` parameter was added.
         *
         */
        do_action('wp_get_template_part', $slug, $name, $templates, $args,$load_once);
        if (!csf_locate_template($templates, true, $load_once, $args)) {
            return false;
        }
    }
}

/**
 * Retrieves the name of the highest priority template file that exists.
 *
 * Searches in the stylesheet directory before the template directory and
 * wp-includes/theme-compat so that themes which inherit from a parent theme
 * can just overload one file.
 *
 * @since 2.7.0
 * @since 5.5.0 The `$args` parameter was added.
 *
 * @global string $wp_stylesheet_path Path to current theme's stylesheet directory.
 * @global string $wp_template_path   Path to current theme's template directory.
 *
 * @param string|array $template_names Template file(s) to search for, in order.
 * @param bool         $load           If true the template file will be loaded if it is found.
 * @param bool         $load_once      Whether to require_once or require. Has no effect if `$load` is false.
 *                                     Default true.
 * @param array        $args           Optional. Additional arguments passed to the template.
 *                                     Default empty array.
 * @return string The template filename if one is located.
 */
if ( ! function_exists( 'csf_locate_template' ) ) {
    function csf_locate_template($template_names, $load = false, $load_once = true, $args = array())
    {
        global $wp_stylesheet_path, $wp_template_path;
        $csfdir = CSF::$dir;
        if (!isset($wp_stylesheet_path) || !isset($wp_template_path)) {
            wp_set_template_globals();
        }

        $is_child_theme = is_child_theme();

        $located = '';
        foreach ((array)$template_names as $template_name) {
            if (!$template_name) {
                continue;
            }
            if (file_exists($wp_stylesheet_path . '/parts/' . $template_name)) {
                $located = $wp_stylesheet_path . '/parts/' . $template_name;
                break;
            } elseif ($is_child_theme && file_exists($wp_template_path . '/parts/' . $template_name)) {
                $located = $wp_template_path . '/parts/' . $template_name;
                break;
            } elseif (file_exists($csfdir . '/parts/' . $template_name)) {
                $located = $csfdir . '/parts/' . $template_name;
                break;
            }
        }


        if ($load && '' !== $located) {
            csf_load_template($located, $load_once, $args);
        }

        return $located;
    }
}
if ( ! function_exists( 'csf_load_template' ) ) {
    function csf_load_template($_template_file, $load_once = true, $args = array())
    {
        global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;

        if (is_array($wp_query->query_vars)) {
            /*
             * This use of extract() cannot be removed. There are many possible ways that
             * templates could depend on variables that it creates existing, and no way to
             * detect and deprecate it.
             *
             * Passing the EXTR_SKIP flag is the safest option, ensuring globals and
             * function variables cannot be overwritten.
             */
            // phpcs:ignore WordPress.PHP.DontExtract.extract_extract
            extract($wp_query->query_vars, EXTR_SKIP);
        }

        if( is_array($args) ){
            extract( $args ); // 这将把参数转换成变量
        }

        if (isset($s)) {
            $s = esc_attr($s);
        }

        /**
         * Fires before a template file is loaded.
         *
         * @param string $_template_file The full path to the template file.
         * @param bool $load_once Whether to require_once or require.
         * @param array $args Additional arguments passed to the template.
         * @since 6.1.0
         *
         */
        do_action('wp_before_load_template', $_template_file, $load_once, $args);

        if ($load_once) {
            get_template_part($_template_file);
            require_once $_template_file;
        } elseif($load_once===false) {
            require $_template_file;
        }else{
            // 读取模板文件的全部内容到一个变量中
            $templateContent = file_get_contents($_template_file);
            // 使用函数替换模板变量
            $templateContent = '?>'.replaceTemplateVariables($templateContent,$args).'<?php ';
            eval($templateContent);
        }


        /**
         * Fires after a template file is loaded.
         *
         * @param string $_template_file The full path to the template file.
         * @param bool $load_once Whether to require_once or require.
         * @param array $args Additional arguments passed to the template.
         * @since 6.1.0
         *
         */
        do_action('wp_after_load_template', $_template_file, $load_once, $args);
    }
}
// 替换模板变量的函数
if ( ! function_exists( 'replaceTemplateVariables' ) ) {
    function replaceTemplateVariables($template, $data): string|array|null
    {
        // 去除多行注释
        $template = preg_replace('/\*(?:[^*]+|(?:\*+[^*])+)\*+/', '', $template);

        // 去除单行注释
        $template = preg_replace('/^\s*\/\/.*\n/m', '', $template);



        foreach ($data as $key => $value) {
            if(!is_array($value)) {
                $template = str_replace(trim('{{' . $key . '}}'), $value, $template);
            }else{
                replaceTemplateVariables($template, $value);
            }
        }

        return $template;
    }
}



